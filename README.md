Naksha
------


Naksha, or more accurately naqshA (नक़्शा), is a Haskell library to work
with maps and related data. We plan to support various operations on
points, trails and other map related objects. We also plan to support
reading, writing and generating various standard data file format.


[![Build Status][status]](https://app.shippable.com/projects/57b87366503ad81000c6d8f8)

[status]: <https://api.shippable.com/projects/57b87366503ad81000c6d8f8/badge?branch=master> "Build Status"
